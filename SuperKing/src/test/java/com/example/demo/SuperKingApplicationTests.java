package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.game.king.SuperKingApplication;

@SpringBootTest(classes = SuperKingApplication.class)
class SuperKingApplicationTests {

	@Test
	void contextLoads() {
	}

}
