package com.game.king.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.game.king.model.GameModel;

public interface GameRepository extends JpaRepository<GameModel, Long>{

}
