package com.game.king.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@Table(name = "NewGame")
public class GameModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "gameName")
	private String name;
	
	@Column(name = "gameTime")
	@Temporal(TemporalType.TIME)
	//@DateTimeFormat(pattern = "hh:mm a")
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "hh:mm a")
	private Date time;

}