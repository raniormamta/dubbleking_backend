package com.game.king.service;

import java.util.List;

import com.game.king.model.GameModel;

public interface GameService {
	GameModel createGame(GameModel gameModel);
	
	GameModel updateGame(GameModel gameModel);

    List <GameModel> getAllGame();

    GameModel getGameById(long gameId);

    void deleteGame(long gameId);
}
