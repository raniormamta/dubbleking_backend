package com.game.king.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.game.king.model.GameModel;
import com.game.king.repositories.GameRepository;

@Service
@Transactional
public class GameServiceImpl implements GameService{
	
	@Autowired
	private GameRepository gameRepository;

	@Override
	public GameModel createGame(GameModel gameModel) {
		
		return gameRepository.saveAndFlush(gameModel);
	}

	@Override
	public GameModel updateGame(GameModel gameModel) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<GameModel> getAllGame() {
		// TODO Auto-generated method stub
		
		return this.gameRepository.findAll();
	}

	@Override
	public GameModel getGameById(long gameId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteGame(long gameId) {
		// TODO Auto-generated method stub
		
	}

}
