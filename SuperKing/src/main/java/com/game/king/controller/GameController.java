package com.game.king.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.game.king.model.GameModel;
import com.game.king.service.GameService;

@RestController
public class GameController {
	
	@Autowired
	private GameService gameService;	
	
	@RequestMapping("/new")
	public String showNewGamePage(Model model) {
		GameModel gameModel = new GameModel();
		model.addAttribute("gameModel", gameModel);
		return "new game is entered";
	} 
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String newTest() {
		return "Hello";
	}
	
	@PostMapping("/games")
    public ResponseEntity<GameModel> createNewGame(@RequestBody GameModel gameModel) {
        return ResponseEntity.ok().body(gameService.createGame(gameModel));
    }
	
	 @GetMapping("/games")
	 public ResponseEntity <List<GameModel>> getAllProduct() {
	        return ResponseEntity.ok().body(gameService.getAllGame());
	    }
}
