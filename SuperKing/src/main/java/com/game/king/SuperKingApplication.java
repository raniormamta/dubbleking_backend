package com.game.king;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperKingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuperKingApplication.class, args);
	}

}
